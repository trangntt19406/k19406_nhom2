const { I } = inject()
const customMethod = require("../../utils/customMethod")
const settingPageLocator = require('./locator')
const timeout = require('../common/timeout');
const { editApiKey } = require("./locator");
const createNewCompanyLocator = require('../createNewCompany/locator')

module.exports = {
    viewCreationPage() {
        customMethod.clickElement(settingPageLocator.profileNameLabel)
        customMethod.clickElement(settingPageLocator.changeCompanyButton)
        customMethod.clickElement(settingPageLocator.addNewButton)
        I.waitForElement(createNewCompanyLocator.titleText, timeout.waitForElement)
    },
    createApiKey(apiName){
        customMethod.clickElement(settingPageLocator.settingTab)
        customMethod.clickElement(settingPageLocator.apiKeysTab)
        customMethod.clickElement(settingPageLocator.apiKeyButton)
        customMethod.fieldValue(settingPageLocator.apiField, apiName)
        I.waitForElement(settingPageLocator.apiKeyLabel, timeout.waitForElement)
        customMethod.clickElement(settingPageLocator.createApiButton)
        I.waitForElement(settingPageLocator.messageCreateSuccessApiKey, timeout.waitForElement)
        customMethod.clickElement(settingPageLocator.doneButton)
    },
    editApiKey(apiName){
        customMethod.clickElement(settingPageLocator.editApiKey)
        customMethod.fieldValue(settingPageLocator.apiField, apiName)
        I.waitForElement(settingPageLocator.apiKeyLabel, timeout.waitForElement)
        customMethod.clickElement(settingPageLocator.editApiButton)
        I.waitForElement(settingPageLocator.editedApiKeyName, timeout.waitForElement)
    },
    deleteApiKey(){
        customMethod.clickElement(settingPageLocator.deleteApiKey)
        customMethod.clickElement(settingPageLocator.deleteButton)
        I.waitForElement(settingPageLocator.deleteSuccessApiKey, timeout.waitForElement)
    }
}
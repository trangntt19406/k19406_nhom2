const timeout = require("../pages/common/timeout")
const { I } = inject()

module.exports = {
    fieldValue(locator, value) {
        I.waitForVisible(locator, timeout.waitForElement)
        I.fillField(locator, value)
    },
    clickElement(locator) {
        I.waitForElement(locator, timeout.waitForElement)
        I.click(locator)
    }
}
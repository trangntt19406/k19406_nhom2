const { I } = inject()
const loginFunction = require('../pages/login/index.js')
const loginLocator = require('../pages/login/locator.js')
const MyVariable = require('../pages/common/variable')

Given('I login to Casso page', () => {
    loginFunction.login(loginLocator.urlLoginPage, MyVariable.email, MyVariable.password)
})
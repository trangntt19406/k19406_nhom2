const { I } = inject()
const settingPageFunction = require('../pages/settingPage/index')
const MyVariable = require('../pages/common/variable')

Given('I edit a api key', () => {
    settingPageFunction.editApiKey(MyVariable.apiFieldEdit)
});
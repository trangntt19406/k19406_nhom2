const { I } = inject()
const settingPageFunction = require('../pages/settingPage/index')
const MyVariable = require('../pages/common/variable')

Given('I create a new api key', () => {
    settingPageFunction.createApiKey(MyVariable.apiField)
});
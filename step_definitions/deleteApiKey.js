const { I } = inject()
const settingPageFunction = require('../pages/settingPage/index')
const MyVariable = require('../pages/common/variable')

Given('I delete a new api key', () => {
    settingPageFunction.deleteApiKey()
});
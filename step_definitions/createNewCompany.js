const { I } = inject()
const createNewCompanyFunction = require('../pages/createNewCompany/index.js')
const createNewCompanyLocator = require('../pages/createNewCompany/locator.js')
const MyVariable = require('../pages/common/variable.js');

Given('I create a new company', () => {
    createNewCompanyFunction.createNewCompany(MyVariable.website, MyVariable.companyName, createNewCompanyLocator.optionsRadio + '['+2+']')
});